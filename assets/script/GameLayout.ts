// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    gameOverLayout = null
    toys: cc.Node[] = []
    gameScore: number = 0
    time: number = 31
    toyList = []
    currentList = []
    heart: cc.Node[] = []

    @property(cc.Prefab)
    toy1: cc.Prefab = null
    @property(cc.Prefab)
    toy2: cc.Prefab = null
    @property(cc.Prefab)
    toy3: cc.Prefab = null
    @property(cc.Prefab)
    toy4: cc.Prefab = null
    @property(cc.Prefab)
    toy5: cc.Prefab = null
    @property(cc.Prefab)
    toy6: cc.Prefab = null
    @property(cc.Prefab)
    toy7: cc.Prefab = null
    @property(cc.Prefab)
    toy8: cc.Prefab = null
    @property(cc.Prefab)
    toy9: cc.Prefab = null
    @property(cc.Prefab)
    toy10: cc.Prefab = null
    @property(cc.Prefab)
    toy11: cc.Prefab = null
    @property(cc.Prefab)
    toy12: cc.Prefab = null
    @property(cc.Prefab)
    toy13: cc.Prefab = null
    @property(cc.Prefab)
    toy14: cc.Prefab = null
    @property(cc.Prefab)
    toy15: cc.Prefab = null
    @property(cc.Prefab)
    toy16: cc.Prefab = null
    @property(cc.Prefab)
    toy17: cc.Prefab = null
    @property(cc.Prefab)
    toy18: cc.Prefab = null

    @property(cc.Prefab)
    heartPrefab: cc.Prefab = null
    @property(cc.Label)
    scoreLbl: cc.Label = null
    @property(cc.Label)
    timeLbl: cc.Label = null

    initHeart(numHeart: number) {
        for (let i = 0;i < numHeart;i++) {
            this.heart[i] = cc.instantiate(this.heartPrefab)
            this.node.getChildByName('Status').getChildByName('Heart').addChild(this.heart[i])
            this.heart[i].x = i * -60
            this.heart[i].y = 0
        }
    }

    // LIFE-CYCLE CALLBACKS:
    onPlay(isReplay: boolean = true) {
        this.initHeart(3)
        this.time = 31
        if (this.node.getChildByName('Toys').childrenCount != 0) {
            this.node.getChildByName('Toys').destroyAllChildren()
            this.currentList = []
        }
        this.gameStatus = GameStatus.Game_Playing
        this.gameScore = 0
        this.scoreLbl.string = this.gameScore.toString()
        this.onSpawn()
        // this.countdown(this.time)
    }

    gameOver() {
        this.node.stopAllActions()
        this.gameStatus = GameStatus.Game_Over
        cc.Canvas.instance.node.getChildByName('GameLayout').active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
        this.currentList = []
        this.node.getChildByName('Toys').destroyAllChildren()
    }

    countdown(time: number) {
        --time
        this.timeLbl.string = time.toString()
        if (time != 0) {
            this.scheduleOnce(() => this.countdown(time), 1)
        } else {
            this.gameOver()
        }
        this.timeLbl.string = time.toString()
    }

    initToy() {
        if (this.currentList.length < 4) {
            let index = Math.floor(Math.random() * this.toyList.length)
            this.currentList.push(this.toyList[index])

            let y = -430
            let randomX = Math.floor(Math.random() * (300 - (-300) + 1)) + (-300)
            let toy = cc.instantiate(this.toyList[index])
            toy.x = randomX
            toy.y = y
            this.node.getChildByName('Toys').addChild(toy)
            this.toys.push(toy)

            this.toyList.splice(index, 1)
        } else {
            // choose random from current list
            let index = Math.floor(Math.random() * this.currentList.length)

            let y = -430
            let randomX = Math.floor(Math.random() * (300 - (-300) + 1)) + (-300)
            let toy = cc.instantiate(this.currentList[index])
            toy.x = randomX
            toy.y = y
            this.node.getChildByName('Toys').addChild(toy)
            this.toys.push(toy)

            this.currentList.push(this.currentList[index])
            this.toyList.push(this.currentList[index])
        }
    }

    onSpawn() {
        if (this.gameStatus == GameStatus.Game_Playing) {
            setTimeout(() => {
                if (this.currentList.length < 6 && this.gameStatus == GameStatus.Game_Playing) {
                    this.initToy()
                    this.onSpawn()
                } else return
            }, 6000)
        }
    }

    onFail() {
        this.node.getChildByName('Status').getChildByName('Heart').removeChild(this.heart[0])
        this.heart.splice(0, 1)
        // this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
    }

    onSuccess() {
        this.gameScore += 50
        this.scoreLbl.string = this.gameScore.toString()
        // this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
    }

    onLoad() {
        var collisionManager = cc.director.getCollisionManager()
        collisionManager.enabled = true
        this.toyList = [
            this.toy1, this.toy2, this.toy3, this.toy4, this.toy5, this.toy6, this.toy7, this.toy8, this.toy9,
            this.toy10, this.toy11, this.toy12, this.toy13, this.toy14, this.toy15, this.toy16, this.toy17, this.toy18
        ]
        this.gameScore = 0
        this.time = 31
        this.scoreLbl.string = this.gameScore.toString()
        this.timeLbl.string = this.time.toString()

        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')

    }

    start() {

    }

    update(dt) {
        if (this.heart.length == 0) {
            this.gameOver()
        }
    }
}
