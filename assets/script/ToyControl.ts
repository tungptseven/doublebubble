// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import { SoundType } from "./AudioSourceControl"
import GameScene from "./GameScene"

const { ccclass, property } = cc._decorator

@ccclass
export default class ToyControl extends cc.Component {
    gameControl: GameScene = null
    gameLayout: GameLayout = null

    // LIFE-CYCLE CALLBACKS:
    onMove() {
        this.node.runAction(cc.moveTo(5, cc.v2(this.node.x, 400)))
    }

    onRemove() {
        this.node.removeFromParent()
        this.node.destroy()
    }

    onCollisionEnter(other, self) {
        if (other.tag === 2) {
            let listName = this.gameLayout.currentList.map(t => t.name)
            if (listName.length == 5) {
                if (listName.indexOf(this.node.name) != -1) {
                    this.gameLayout.currentList = this.gameLayout.currentList.filter(t => t.name !== this.node.name)
                    this.gameLayout.onFail()
                    this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
                }
            }
            this.onRemove()
        }
    }

    onClick() {
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Bubble)
        let listName = this.gameLayout.currentList.map(t => t.name)
        if (listName.length == 5) {
            if (listName.indexOf(this.node.name) == -1) {
                this.gameLayout.currentList = this.gameLayout.currentList.filter(t => t.name !== this.node.name)
                this.gameLayout.onFail()
            } else {
                this.gameLayout.currentList = this.gameLayout.currentList.filter(t => t.name !== this.node.name)
                this.gameLayout.onSuccess()
                this.onRemove()
            }
        } else {
            this.gameLayout.currentList = this.gameLayout.currentList.filter(t => t.name !== this.node.name)
            this.gameLayout.onFail()
            this.onRemove()
        }
    }

    onLoad() {
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
        this.node.runAction(cc.repeatForever(
            cc.sequence(
                cc.rotateTo(0.5, 4),
                cc.rotateTo(0.5, -4)
            )
        ))
        setTimeout(() => this.onMove(), 1000)
        if (this.node.on('touchend', this.onClick, this)) { }
    }

    start() {

    }

    // update (dt) {}
}
